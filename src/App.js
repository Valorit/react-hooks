import React, {useState, useEffect} from 'react'
import TodoList from './TodoList'

export default function App() {
  const [todos,
    setTodos] = useState([]) //useState - всегда массив из двух элементов, где первый элемент это состояние, второй элемент это функция, позволяющая менять состояние

  const [todoTitle,
    setTodoTitle] = useState('')

  const handleClick = () => console.log('click') // б. вывод в консоль при клике

  useEffect(() => { //useEffect - первый параметр callback, который будет выполняться, второй параметр список зависимостей на которые откликается данный хук
    const raw = localStorage.getItem('todos') || []
    setTodos(JSON.parse(raw))
  }, [])

  useEffect(() => { 
    document.addEventListener('click', handleClick) // а. пример создания слушателя
    localStorage.setItem('todos', JSON.stringify(todos)) //при изменении todos сохраняется в local storage
    return () => {
      document.removeEventListener('click', handleClick) // в. предотвращение утечки памяти связанные со слушателем useEffect
    } 
  }, [todos]) 

  const addTodo = event => {
    if (event.key === 'Enter') {
      setTodos([
        ...todos, {
          id: Date.now(),
          title: todoTitle,
          completed: false
        }
      ])
      setTodoTitle('')
    }
  }

  return (
    <div className="container">
      <h1>Todo app</h1>

      <div className="input-field">
        <input
          type="text"
          value={todoTitle}
          onChange=
          {event=>setTodoTitle(event.target.value)}
          onKeyPress={addTodo}/>
        <label>Todo name</label>
      </div>

      <TodoList todos={todos}/>
    </div>
  );
}